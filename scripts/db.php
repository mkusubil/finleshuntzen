<?php

global $db;
$db = new PDO('mysql:host=localhost;dbname=u20362', 'u20362','5800777', array(PDO::ATTR_PERSISTENT => true));

function db_require($command,$params = NULL){
    global $db;
    try {
        $stmt = $db->prepare($command);
        $stmt->execute($params);
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    return $stmt;
}

function db_require_fetch($command,$params = NULL){
    return db_require($command,$params)->fetchAll();
}

function db_require_rows($command,$params = NULL){
   return  db_require($command,$params)->rowCount();
}
function db_last_insid(){
    global $db;
    return $db->lastInsertId();
}
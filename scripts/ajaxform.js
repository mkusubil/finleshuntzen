document.addEventListener('DOMContentLoaded', () => {

    const ajaxSend = (formData) => {
        fetch('index.php', { // файл-обработчик 
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json', // отправляемые данные 
            },
            body: JSON.stringify(formData)
        })
            .then(response => alert('Сообщение отправлено'))
            .catch(error => console.error(error))
    };

    const forms = document.getElementsByTagName('form');
    for (let i = 0; i < forms.length; i++) {
        forms[i].addEventListener('submit', function (e) {
            e.preventDefault();
            
            let formData = {};
            $('#tx,select,input:checkbox:checked,input:radio:checked').each(function() {
            	  formData[this.name] = $(this).val();
            	});
        	console.log(formData);
            
            ajaxSend(formData);
            this.reset();
        });
    };
});
<?php

function valid_post($r){
    $errors = FALSE;
    if (empty($r['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else if(!preg_match('/^[а-яА-Я ]+$/u', $r['fio'])){
        setcookie('fio_error', '2', time() + 30 * 24 * 60 * 60);
        $errors = TRUE;
    }  else {
        setcookie('fio_value', $r['fio'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($r['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else if(!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $r['email']))
    {
        setcookie('email_error', '2', time() + 30 * 24 * 60 * 60);
        $errors = TRUE;
    }  else {
        setcookie('email_value', $r['email'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($r['bday'])) {
        setcookie('bday_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else if(!preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $r['bday']))
    {
        setcookie('bday_error', '2', time() + 30 * 24 * 60 * 60);
        $errors = TRUE;
    }  else {
        setcookie('bday_value', $r['bday'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($r['sex'])) {
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else if(!preg_match('/^WOM|MAN$/', $r['sex']))
    {
        setcookie('sex_error', '2', time() + 30 * 24 * 60 * 60);
        $errors = TRUE;
    }  else {
        setcookie('sex_value', $r['sex'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($r['lim']) && $r['lim']!='0') {
        setcookie('lim_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else if(!preg_match('/^[0-4]$/', $r['lim']))
    {
        setcookie('lim_error', '2', time() + 30 * 24 * 60 * 60);
        $errors = TRUE;
    }  else {
        setcookie('lim_value', $r['lim'], time() + 30 * 24 * 60 * 60);
    }
    
    $ability_labels = ['god' => 'Бессмертие', 'twalk' => 'Прохождение сквозь стены', 'fly' => 'Левитация' ];
    $ability_data = array_keys($ability_labels);
    $error_ab = FALSE;
    if (empty($r['abilities'])) {
        setcookie('abil_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
        $error_ab = TRUE;
    }
    else{
        $abilities = $r['abilities'];
        foreach ($abilities as $ability) {
            if (!in_array($ability, $ability_data)) {
                setcookie('abil_error', '2', time() + 24 * 60 * 60);
                $errors = TRUE;
                $error_ab = TRUE;
            }
        }
    }
    if(!$error_ab) {
        $ability_insert = [];
        foreach ($ability_data as $ability) {
            $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
            setcookie('abil_value', serialize($ability_insert) , time() + 30 * 24 * 60 * 60);
        }
    }
    
    if (empty($r['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('bio_value', $r['bio'], time() + 30 * 24 * 60 * 60);
    }
    
    if (!empty($r['yess']) && $r['yess']!='1' )
    {
        setcookie('yes_error', '1', time() + 30 * 24 * 60 * 60);
        $errors = TRUE;
    }  else {
        if(empty($r['yess'])) $r['yess']='2';
        setcookie('yes_value', $r['yess'], time() + 30 * 24 * 60 * 60);
    }
    if (!isset($ability_insert)) $ability_insert="";
    return array('er'=> $errors,'ab' => $ability_insert);
}

function valid_token_authed($r,$ability_insert) {
            if($_SESSION['token'] != $r['client-token']){
                setcookie('token_error', '1', time() + 30 * 24 * 60 * 60);
                if($_SERVER['CONTENT_TYPE'] != 'application/json')
                    header('Location: index.php');
                exit();
            }
            $r['bday']=strtotime($r['bday']);
            if($r['yess'] == 2) $r['yess'] = 0;
            $params=array($r['fio'],$r['email'],$r['bday'],$r['sex'],$r['lim'],$ability_insert['god'],$ability_insert['twalk'],$ability_insert['fly'],$r['bio'],$r['yess'],$_SESSION['uid']);
            $require="UPDATE Autouser SET fio = ?,email = ?,bday = ?,sex = ?,lim = ?,god = ?,twalk = ?,fly = ?,bio = ?,yess = ?  WHERE id = ?";
            db_require($require,$params);
            return 1;
        }
        function valid_unauthed($r,$ability_insert) {
    
    $pass=substr(md5(uniqid()),0,8);
    $require ="INSERT INTO Autouser SET password = md5(?)";
    $params=array($pass);
    db_require($require,$params);
    
    $current_id=db_last_insid();
    $login = 'user'.$current_id;
    $require="UPDATE Autouser SET login = ? WHERE id = ?";
    $params=array($login,$current_id);
    db_require($require,$params);
    
    $r['bday']=strtotime($r['bday']);
    if($r['yess'] == 2) $r['yess'] = 0;
    if($r['yess'] == "") $r['yess'] = 0;
    
    print_r($r);
    $require="UPDATE Autouser SET fio = ?,email = ?,bday = ?,sex = ?,lim = ?,god = ?,twalk = ?,fly = ?,bio = ?,yess = ?  WHERE id = ?";
    $params=array($r['fio'],$r['email'],$r['bday'],$r['sex'],$r['lim'],$ability_insert['god'],$ability_insert['twalk'],$ability_insert['fly'],$r['bio'],$r['yess'],$current_id);
    db_require($require,$params);
    
    setcookie('login', $login);
    setcookie('pass', $pass);
}
function del_tokens(){
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('bday_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('lim_error', '', 100000);
    setcookie('abil_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('yes_error', '', 100000);
    setcookie('token_error', '', 100000);
}
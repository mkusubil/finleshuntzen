<!DOCTYPE html>
<html lang="ru">
  <head>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
      .error {
      border: 2px solid red;
      }
      .error1{
      color:red;
      font-weight: bold;
      }
      .mesa {
      font-weight: bold;
      }
      #messages{
      margin-left: 32vh;
      }
    </style>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <title>Exercice 4</title>
  </head>
  <body>
    <header>
      <div class="container-fluid justify-content-center p-0">
        <div class="row align-items-center px-0 justify-content-md-center color1 ">
          <div class="col-2 pl-md-0 ml-md-n4 ">
            <img src="https://flink.apache.org/img/logo/png/500/flink_squirrel_500.png" alt="logo" id="logo">
          </div>
          <div class="col-auto ml-5 ml-md-0  text-center">
            <h1>Форма</h1>
          </div>
        </div>
      </div>
      </header>
<?php
if (!empty($c['m'])) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($c['m'] as $c['m']) {
      print($c['m']);
  }
  print('</div>');

}
?>
<script src="scripts/jquery-3.5.1.min.js"></script>
<script src ="scripts/ajaxform.js"></script>
      <div class="table & form">
          <div class="col-sm-12 col-md-6 mx-sm-0  mx-md-auto color3 order-md-0 mt-2  ">
            <h2>Форма</h2>
            <?php  if(empty($c['s']['login'])) print('
            <a href="login">
        <button type="button" class="btn btn-primary" >Авторизация</button>
        </a>');
        ?>
        <?php  if(empty($c['s']['login'])) print('
            <a href="admin">
        <button type="button" class="btn btn-secondary" >Администрирование</button>
        </a>');
        ?>
            <form action="" method="POST">
              <label>
              Имя
              <br>
              <input name="fio" id="tx" <?php if ($c['e']['fio']) {print 'class="error"';} ?> value="<?php print $c['v']['fio']; ?>" placeholder="Name">
              </label>
              <br>
              <label>
              Email
              <br>
              <input name="email" id="tx" <?php if ($c['e']['email']) {print 'class="error"';} ?> value="<?php print $c['v']['email']; ?>" type="email" placeholder="yourmail@gmail.com">
              </label>
              <br>
              <label>
              Дата рождения
              <br>
              <input name="bday" id="tx" <?php if ($c['e']['bday']) {print 'class="error"';} ?> value="<?php print $c['v']['bday']; ?>" type="date">
              </label>
              <br>
              <p>
                Пол
              </p>
              <br>
              <label <?php if ($c['e']['sex']) {print 'class="error1"';} ?>>
              <input type="radio" <?php if($c['v']['sex'] == "MAN") {print 'checked="checked"';}?> name="sex" value="MAN"> М
              </label>
              <label <?php if ($c['e']['sex']) {print 'class="error1"';} ?>>
              <input type="radio" <?php if($c['v']['sex'] == "WOM") {print 'checked="checked"';}?> name="sex" value="WOM"> Ж
              </label>
              <br>
              <p>
                Количество конечностей
              </p>
              <br>
              <label <?php if ($c['e']['lim']) {print 'class="error1"';} ?>>
              <input type="radio" <?php if($c['v']['lim'] == "4") {print 'checked="checked"';}?> name="lim" value="4"> 4
              </label>
              <label <?php if ($c['e']['lim']) {print 'class="error1"';} ?>>
              <input type="radio"  <?php if($c['v']['lim'] == "3") {print 'checked="checked"';}?> name="lim" value="3"> 3
              </label>
              <label <?php if ($c['e']['lim']) {print 'class="error1"';} ?>>
              <input type="radio"  <?php if($c['v']['lim'] == "2") {print 'checked="checked"';}?> name="lim" value="2"> 2
              </label>
              <label <?php if ($c['e']['lim']) {print 'class="error1"';} ?>>
              <input type="radio"  <?php if($c['v']['lim'] == "1") {print 'checked="checked"';}?> name="lim" value="1"> 1
              </label>
              <label <?php if ($c['e']['lim']) {print 'class="error1"';} ?>>
              <input type="radio"  <?php if($c['v']['lim'] == "0" && (isset($_COOKIE['lim_value'])||isset($user_data[0]['lim']))) {print 'checked="checked"';}?> name="lim" value="0"> 0
              </label>
              <br>
              <label>
                Суперспособности
                <br>
                <select name="abilities[]" multiple="multiple" <?php if ($c['e']['abil']) {print 'class="error"';} ?>>
                  <option  <?php if($c['v']['god'] == "1") {print 'selected="selected"';}?> value="god">Бессмертие</option>
                  <option  <?php if($c['v']['twalk'] == "1") {print 'selected="selected"';}?>value="twalk">Прохождение сквозь стены</option>
                  <option  <?php if($c['v']['fly'] == "1") {print 'selected="selected"';}?>value="fly">Левитация</option>
                </select>
              </label>
              <br>
              <label>
              Биография
              <br>
              <textarea name="bio" id="tx"  <?php if ($c['e']['bio']) {print 'class="error"';} ?> placeholder="Your biography"><?=$c['v']['bio'];?></textarea>
              </label>
              <br>
              <label <?php if ($c['e']['yes']) {print 'class="error1"';} ?>>
              <input type="checkbox" <?php if($c['v']['yes'] == "1") {print 'checked="checked"';}?> name="yess" value="1"> С контрактом ознакомлен
              </label>
              <br>
              <?php if(!empty($c['s']['token']))
                  print '<input type ="hidden" name="client-token" id="tx" value="'.$c['s']['token'].'">'?>
              <button class="btn btn-success">Отправить</button>
            </form>
          </div>
          <div class="col-0 col-md-1">
          </div>
        </div>
    <footer>
      <div class="row color3 mt-2">
        <div class="col-12 ">
          <h2>
            (c)Матвей Скирда
          </h2>
        </div>
      </div>
    </footer>
  </body>
</html>

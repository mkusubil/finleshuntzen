<?php

// auth_basic.php - HTTP Authentication Script v 1.0
//##################################################

function auth(&$request, $r) {
  // TODO: запрашивать пользователя из БД.
  
    if (preg_match('/^[a-z]+$/u',$_SERVER['PHP_AUTH_USER']) && preg_match('/^[a-zA-Z0-9]+$/u',$_SERVER['PHP_AUTH_PW'])){
        $require="SELECT * FROM Admin WHERE adlog=? AND adpass=md5(?)";
        $params=array($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW']);
        $count=db_require_rows($require,$params);
    
    }
    else $count=0;
  
        
  if (!isset($_SERVER['PHP_AUTH_USER']) ||!isset($_SERVER['PHP_AUTH_PW']) || $count == 0) {
    $response = array(
      'headers' => array(sprintf('WWW-Authenticate: Basic realm="%s"', conf('sitename')), 'HTTP/1.0 401 Unauthorized'),
      'entity' => theme('401', $request),
    );
    return $response;
  }
}

<?php
session_start();
//TODO : выход из сессии
function login_get($request){ 
    $login_value ='';
    if(isset($_COOKIE['autor_error'])){
        print('Ошибка авторизации. Побробуйте снова');
        $login_value= isset($_COOKIE['login_value']) ? strip_tags($_COOKIE['login_value']) : '';
    }
    setcookie('autor_error', '', 100000); 
    setcookie('login_value', '', 100000);
    $params = ['log_v' => $login_value];
    return theme('login',$params);
}
 function login_post($request){
  $require ="SELECT id FROM Autouser WHERE login = ? AND password = md5(?)";
  $params=array($request['post']['login'], $request['post']['pass']);
  $user_id = db_require_fetch($require,$params);
  
  if (!empty($user_id[0]['id'])) {
    setcookie('autor_error', '', 100000);
    setcookie('login_value', '', 100000);
    // Если все ок, то авторизуем пользователя.
    $_SESSION['login'] = $_POST['login'];
    // Записываем ID пользователя.
    $_SESSION['uid'] = $user_id[0]['id'];
    // Делаем перенаправление.
    
    $token=substr(md5(uniqid()),0,8).$_SERVER['REMOTE_ADDR'].$_SESSION['login'];
    $_SESSION['token'] = $token;
   return redirect('index.php');
  }
  else {
    setcookie('autor_error', '1', time() + 24 * 60 * 60);
    setcookie('login_value',$request['post']['login'], time() + 24 * 60 * 60);
    // Делаем перенаправление.
    return redirect('login');
  }  
}

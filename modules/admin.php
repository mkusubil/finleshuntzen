<?php

// Обработчик запросов методом GET.
function admin_get($request) {
  // Достаем данные из БД, форматируем, санитизуем, складываем в массив, передаем в шаблон для вывода в HTML.
    $require ="SELECT * FROM Autouser";
    $users_data = db_require_fetch($require);
    $values=[];
    foreach ($users_data as $row){
        $fio = strip_tags($row['fio']);
        $email = strip_tags($row['email']);
        $bday = date("Y-m-d",intval($row['bday']));
        $sex= $row['sex'] == 'MAN'? 'муж.': 'жен.';
        $lim=(int)$row['lim'];
        $abilites=[];
        if(!empty($row['god'])){
            $abilites[]='Бессмертие';
        }
        if(!empty($row['twalk'])){
            $abilites[]='Прохождение сквозь стены';
        }
        if(!empty($row['fly'])){
            $abilites[]='Левитация';
        }
        $abilites=implode(',',$abilites);
        $bio=strip_tags($row['bio'],'<a>');
        $yes= $row['yess'] == '1'? 'Да' :'Нет';
        
        
        $values[$row['id']]= [
            $fio,
            $email,
            $bday,
            $sex,
            $lim,
            $abilites,
            $bio,
            $yes
        ];
    }
    print('Вы успешно авторизовались и видите защищенные паролем данные.');
    $values_lables=['Имя','Email','Дата рождения','Пол','Количество конечностей','Способности','Биография','Ознакомлен с контрактом','Удалить'];
    $params= ['lables' => $values_lables, 'values' => $values];
  return theme('admin',$params);
}

// Обработчик запросов методом POST.
function admin_post($request) {
    $params=array($request['post']['butdel']);
    $require ="DELETE FROM Autouser WHERE id = ?";
    db_require($require,$params);
    return redirect('admin');
}

<?php

// Обработчик запросов методом GET.
function front_get($request) {
    $messages = array();
    
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('Вы можете <a href="login">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }
    
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['bday'] = !empty($_COOKIE['bday_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['lim'] = !empty($_COOKIE['lim_error']);
    $errors['abil'] = !empty($_COOKIE['abil_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['yes'] = !empty($_COOKIE['yes_error']);
    
    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        if($_COOKIE['fio_error'] == '1')
            $messages[] = '<div class="mesa">Заполните Имя.</div>';
            else $messages[] = '<div class="mesa">Недопустимые символы в Имени</div>';
    }
    
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        if($_COOKIE['email_error'] == 1)
            $messages[] = '<div class="mesa">Заполните Email.</div>';
            else $messages[] = '<div class="mesa">Недопустимые символы в Email</div>';
    }
    
    if ($errors['bday']) {
        setcookie('bday_error', '', 100000);
        if($_COOKIE['bday_error'] == 1)
            $messages[] = '<div class="mesa">Заполните Дату рождения.</div>';
            else $messages[] = '<div class="mesa">Недопустимый формат Дня рождения </div>';
    }
    
    if ($errors['sex']) {
        setcookie('sex_error', '', 100000);
        if($_COOKIE['sex_error'] == 1)
            $messages[] = '<div class="mesa">Выберете Пол.</div>';
            else $messages[] = '<div class="mesa">Недопустимый формат Пола </div>';
    }
    
    
    if ($errors['lim']) {
        setcookie('lim_error', '', 100000);
        if($_COOKIE['lim_error'] == 1)
            $messages[] = '<div class="mesa">Выберете Количество конечностей.</div>';
            else $messages[] = '<div class="mesa">Недопустимый формат Количество конечностей.</div>';
    }
    
    if ($errors['abil']) {
        setcookie('abil_error', '', 100000);
        if($_COOKIE['abil_error'] == 1)
            $messages[] = '<div class="mesa">Выберете Способность.</div>';
            else $messages[] = '<div class="mesa">Недопустимый формат Способности.</div>';
    }
    
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="mesa">Заполните биографию.</div>';
    }
    
    if ($errors['yes']) {
        setcookie('yes_error', '', 100000);
        $messages[] = '<div class="mesa">Неверное значение С контрактом ознакомлен</div>';
    }
    
    if(isset($_COOKIE['token_error'])){
        setcookie('token_error', '', 100000);
        $messages[] = '<div class="mesa">Ошибка отправки формы</div>';
    }
    
    $values = array();
    
    if (isset($_COOKIE['fio_value']))
        $values['fio'] = !preg_match('/^[а-яА-Я ]+$/u',$_COOKIE['fio_value']) || empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
        else $values['fio']='';
        
        if (isset($_COOKIE['email_value']))
            $values['email'] = !preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u'
                ,$_COOKIE['email_value']) || empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
                else $values['email']='';
                
                if (isset($_COOKIE['bday_value']))
                    $values['bday'] = !preg_match('/^\d\d\d\d\-\d\d\-\d\d$/' ,$_COOKIE['bday_value']) || empty($_COOKIE['bday_value']) ? '' : $_COOKIE['bday_value'];
                    else $values['bday']='';
                    
                    if (isset($_COOKIE['sex_value']))
                        $values['sex'] = !preg_match('/^WOM|MAN$/' ,$_COOKIE['sex_value']) || empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
                        else $values['sex']='';
                        
                        if (isset($_COOKIE['lim_value']))
                            $values['lim'] = !preg_match('/^[0-4]$/' ,$_COOKIE['lim_value']) || empty($_COOKIE['lim_value']) ? '0' : $_COOKIE['lim_value'];
                            else $values['lim']='0';
                            
                            if (isset($_COOKIE['abil_value'])){
                                $abiln=unserialize($_COOKIE['abil_value']);
                                $values['god'] = !preg_match('/^[0-1]$/' , $abiln['god']) || empty($abiln['god']) ? '0' : $abiln['god'];
                                $values['twalk'] = !preg_match('/^[0-1]$/' ,$abiln['twalk']) || empty($abiln['twalk']) ? '0': $abiln['twalk'];
                                $values['fly'] = !preg_match('/^[0-1]$/' ,$abiln['fly']) || empty($abiln['fly']) ? '0': $abiln['fly'];
                                
                            }
                            else {
                                $values['god']='0';
                                $values['twalk']='0';
                                $values['fly']='0';
                            }
                            
                            if (isset($_COOKIE['bio_value']))
                                $values['bio'] =  empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
                                else $values['bio']='';
                                
                                if (isset($_COOKIE['yes_value']))
                                    $values['yes'] = $_COOKIE['yes_value'];
                                    else $values['yes']='0';
                                    
                                    
                                    if (empty(array_filter($errors)) && !empty($_COOKIE[session_name()]) &&
                                        session_start() && !empty($_SESSION['login'])) {
                                            // Загрузить данные пользователя из БД.
                                            printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
                                            
                                            $require="SELECT * FROM Autouser WHERE id = ?";
                                            $params=array($_SESSION['uid']);
                                            $user_data = db_require_fetch($require,$params);
                                            
                                            $values['fio'] = !empty($user_data[0]['fio']) ? strip_tags($user_data[0]['fio']) : '';
                                            $values['email'] = !empty($user_data[0]['email']) ? strip_tags($user_data[0]['email']) : '';
                                            $values['bday'] = !empty($user_data[0]['bday']) ? date("Y-m-d",$user_data[0]['bday']) : '';
                                            $values['sex'] = !empty($user_data[0]['sex']) ? strip_tags($user_data[0]['sex']) : '';
                                            $values['lim'] = !empty($user_data[0]['lim']) ? $user_data[0]['lim'] : '0';
                                            $values['god'] = !empty($user_data[0]['god']) ? $user_data[0]['god'] : '';
                                            $values['twalk'] = !empty($user_data[0]['twalk']) ? $user_data[0]['twalk'] : '';
                                            $values['fly'] = !empty($user_data[0]['fly']) ? $user_data[0]['fly'] : '';
                                            $values['bio'] = !empty($user_data[0]['bio']) ? strip_tags($user_data[0]['bio']) : '';
                                            $values['yes'] = !empty($user_data[0]['yess']) ? $user_data[0]['yess'] : '';
                                        }
  
  $l= !empty($_SESSION['login']) ? $_SESSION['login'] : '';
  $t= !empty($_SESSION['token']) ? $_SESSION['token'] : '';
  $session = [ 'login' => $l, 'token' => $t];
  $params=array('v' => $values, 'e' => $errors, 'm' => $messages, 's' => $session);                                   
  return theme('front',$params);
  // Пример возврата контента.
  return '123';
  // Пример запрета доступа.
  return access_denied();
  // Пример возврата ресурс не найден.
  return not_found();
}

// Обработчик запросов методом POST.
function front_post($request) {
    if ($_SERVER['CONTENT_TYPE'] == 'application/json'){       
        del_tokens();
        $request['post'] = json_decode(file_get_contents("php://input"), true);
        $request['post']['abilities'] = $request['post']['abilities[]'];
        unset($request['post']['abilities[]']);
        if(!isset($request['post']['yess']))  $request['post']['yess']="";
        if(!isset($request['post']['lim']))  $request['post']['lim']="";
            
        $resp = valid_post($request['post']);
        $errors= $resp['er'];
        $ability_insert = $resp['ab'];
        print_r($errors);
        if ($errors) {
            exit();
        }
        if (!empty($_COOKIE[session_name()]) &&
            session_start() && !empty($_SESSION['login'])) valid_token_authed($request['post'],$ability_insert);
            else valid_unauthed($request['post'],$ability_insert);      
        exit();
    }   
    else{
        
        $resp = valid_post($request['post']);
        $errors= $resp['er'];
        $ability_insert = $resp['ab'];
        
        if ($errors) {
        header('Location: index.php');
        exit();
        }
        else del_tokens();
        
        if (!empty($_COOKIE[session_name()]) &&
            session_start() && !empty($_SESSION['login'])) valid_token_authed($request['post'],$ability_insert);
            else valid_unauthed($request['post'],$ability_insert);      
        setcookie('save', '1');

  return redirect();
    }
}